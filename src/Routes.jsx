import React from "react";
import { Route, Switch } from "react-router-dom";

import Home from "./components/Home/Home";
import Favorites from "./components/Favorites/Favorites";
import News from "./components/News/News";
import NotFound from "./components/NotFound/NotFound";

const Routes = () => (
  <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/favorites" exact component={Favorites} />
    <Route path="/countries" exact component={News} />
    <Route component={NotFound} />
  </Switch>
);

export default Routes;
