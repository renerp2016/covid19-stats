import CountryTable from './CountryTable';
import { connect } from 'react-redux';

const mapStateToProps = state => {
    return {
        data: state.countryData
    }
};

export default connect(mapStateToProps)(CountryTable)