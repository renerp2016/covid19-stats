import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import './CountryTable.scss';

function CountryTable({data = {}}) {
    return data ? (
        <div  className="countryTable">
            <hr/>
            <table className="countryTable__table table table-light">
                <thead>
                <tr>
                    <th colSpan="2" className="text-center font-weight-bold text-uppercase">
                        <span>{moment(data.time).format('MMMM Do YYYY, h:mm:ss a')}</span>
                        <hr/>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Casos activos</th>
                    <td>{data.cases.active}</td>
                </tr>
                <tr>
                    <th className="text-warning">Casos nuevos</th>
                    <td>{data.cases.new}</td>
                </tr>
                <tr>
                    <th className="text-success">Recuperados</th>
                    <td>{data.cases.recovered}</td>
                </tr>
                <tr>
                    <th className="text-danger">Muertes nuevas</th>
                    <td>{data.deaths.new}</td>
                </tr>
                <tr className="font-weight-bold">
                    <th className="text-danger text-uppercase">Total Muertos</th>
                    <td>{data.deaths.total}</td>
                </tr>
                <tr className="font-weight-bold">
                    <th className="text-uppercase">Total Casos</th>
                    <td>{data.cases.total}</td>
                </tr>
                </tbody>
            </table>
            <hr/>
        </div>

    ) : (<h3>NO hay datos que mostrar!</h3>)
}

CountryTable.propTypes = {
    data: PropTypes.object
};

export default CountryTable;