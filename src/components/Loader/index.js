import Loader from './Loader';
import {connect} from 'react-redux';

const mapStateToProps = (state) => ({
        loading: state.loading
    });

export default connect(mapStateToProps)(Loader);
