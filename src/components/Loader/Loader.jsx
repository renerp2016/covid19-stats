import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import PropTypes from 'prop-types';

import './Loader.scss';

const Loader = ({loading = false}) => (
    loading && (<div className="loader">
        <FontAwesomeIcon className="loader__spinner" icon={faCircleNotch} pulse/>
    </div>)
);

Loader.propTypes = {
    loading: PropTypes.bool
};

export default Loader;