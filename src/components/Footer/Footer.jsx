import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome, faStar, faNewspaper } from "@fortawesome/free-solid-svg-icons";
import "./Footer.scss";
import { NavLink } from "react-router-dom";

const buttons = [
  { link: "/", icon: faHome },
  { link: "/favorites", icon: faStar },
  { link: "/news", icon: faNewspaper }
];

const Footer = () => (
  <div className="Footer">
    {buttons.map((btn, index) => (
      <NavLink
        exact
        to={btn.link}
        activeClassName="active"
        className="Footer__btn"
        key={index}
      >
        <FontAwesomeIcon className="Footer__icon" icon={btn.icon} />
      </NavLink>
    ))}
  </div>
);

export default Footer;
