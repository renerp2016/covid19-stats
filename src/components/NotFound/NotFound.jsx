import React from "react";
import "./NotFound.scss";

const NotFound = () => (
  <div className="NotFound">
    <h2 className="NotFound__title">
      <b>404</b>&nbsp;<div>No existe</div>
    </h2>
  </div>
);

export default NotFound;
