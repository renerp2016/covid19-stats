import CountrySelect from './CountrySelect';
import { fetchData } from '../../actions';
import { connect } from 'react-redux';

const mapStateToProps = state => {
    return {
        errors: state.errors,
        countries: state.countries
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onCountryChange(country) {
            dispatch(
                fetchData(country)
            )
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(CountrySelect)