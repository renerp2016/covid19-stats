import React from 'react';
import PropTypes from 'prop-types';

import './CountrySelect.scss';

const CountrySelect = ({countries=[],onCountryChange=f=>f}) => (
    <select className="countrySelect" onChange={(evt)=>onCountryChange(evt.target.value)}>
        {countries.map((country,index)=>(
            <option className="countrySelect__option" value={country} key={index}>{country}</option>
        ))}
    </select>
);

CountrySelect.propTypes = {
    countries:PropTypes.array,
    onCountryChange: PropTypes.func
};

export default CountrySelect;
