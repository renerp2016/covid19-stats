import React from 'react';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from '@fortawesome/free-solid-svg-icons'

import './ErrorList.scss';

const ErrorList = ({errors = [], onCloseError}) => (
    errors.length > 0 && (
        <ul className="errorList">
            {errors.map((error, index) => (
                <li className="errorList__item bg-danger" key={index} onClick={()=>onCloseError(index)}>
                    <span className="errorList__errorMessage">{error}</span>
                    <FontAwesomeIcon className="errorList__errorClose" icon={faTimes}/>
                </li>
            ))}
        </ul>
    )
);

ErrorList.propTypes = {
    errors: PropTypes.array,
    onCloseError: PropTypes.func
};

export default ErrorList;
