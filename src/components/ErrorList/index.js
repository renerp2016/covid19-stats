import {connect} from 'react-redux';
import ErrorList from './ErrorList';
import {clearError} from "../../actions";

const mapStateToProps = (state) => ({
    errors: state.errors
});
const mapDispatchToProps = dispatch => ({
    onCloseError(index) {
        dispatch(
            clearError(index)
        )
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ErrorList)