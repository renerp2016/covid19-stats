import React from "react";
import "./Home.scss";
import CountrySelect from "../CountrySelect";
import CountryTable from "../CountryTable";

const Home = () => (
  <div className="Home">
    <h1 className="mb-4 font-weight-bold">Seleccione un país</h1>
    <CountrySelect />
    <CountryTable />
  </div>
);

export default Home;
