import React from "react";
import "./FlaticonRef.scss";

const FlaticonRef = () => (
  <div className="FlaticonRef">
    Iconos diseñados por{" "}
    <a href="https://www.flaticon.es/autores/freepik" title="Freepik">
      Freepik
    </a>{" "}
    from{" "}
    <a href="https://www.flaticon.es/" title="Flaticon">
      www.flaticon.es
    </a>
  </div>
);

export default FlaticonRef;
