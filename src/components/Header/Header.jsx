import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import "./Header.scss";

import logo from "../../assets/logo.png";
const title = "Covid Stats";

const Header = () => (
  <div className="Header">
    <img src={logo} className="Header__logo" alt="Covid Stats logo" />
    <h1 className="Header__title">{title}</h1>
    <button className="Header__btn">
      <FontAwesomeIcon className="Header__icon" icon={faBars} />
    </button>
  </div>
);

export default Header;
