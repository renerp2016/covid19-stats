import {
    ADD_ERROR,
    CLEAR_ERROR,
    CLEAR_ERRORS,
    RETRIEVE_COUNTRY_DATA,
    RETRIEVE_COUNTRY_DATA_FULFILLED,
    CANCEL_FETCHING,
    ADD_COUNTRY
}
    from '../constants';
import {combineReducers} from 'redux';

export const errors = (state = [], action) => {
    switch (action.type) {
        case ADD_ERROR:
            return [
                ...state,
                action.payload
            ];
        case CLEAR_ERROR:
            const errors = state.filter((error, index) => index !== action.payload)
            return errors;
        case CLEAR_ERRORS:
            return [];
        default:
            return state;
    }
};

export const loading = (state = false, action) => {
    switch (action.type) {
        case  RETRIEVE_COUNTRY_DATA:
            return true;
        case  RETRIEVE_COUNTRY_DATA_FULFILLED:
            return false;
        case CANCEL_FETCHING:
            return false;
        default:
            return state
    }
};

export const countryData = (state = null, action) => {
    switch (action.type) {
        case RETRIEVE_COUNTRY_DATA_FULFILLED:
            return action.payload;
        default:
            return state;
    }
};

export const countries = (state = [], action) => {
    switch (action.type) {
        case ADD_COUNTRY:
            return [
                ...state,
                action.payload
            ];
        default:
            return state;
    }
};

export default combineReducers({
    errors,
    loading,
    countryData,
    countries
})




