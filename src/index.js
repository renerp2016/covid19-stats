import storeFactory from './store'
import {Provider} from 'react-redux'
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import sampleData from './initialState';
import {addError} from "./actions";

import './index.scss';

const LOCAL_STORAGE_NAME = 'covid19-pwa_store';
const initialState = (localStorage[LOCAL_STORAGE_NAME]) ?
    JSON.parse(localStorage[LOCAL_STORAGE_NAME]) :
    sampleData;

const saveState = () =>
    localStorage[LOCAL_STORAGE_NAME] = JSON.stringify(store.getState());

const handleError = error => {
    store.dispatch(
        addError(error.message)
    )
};

const store = storeFactory(initialState);
store.subscribe(saveState);

window.addEventListener('error', handleError);

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
