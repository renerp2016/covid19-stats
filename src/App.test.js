import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import {Provider} from 'react-redux';
import storeFactory from "./store";
import initialState from './initialState';

const store = storeFactory(initialState);

test('renders Flaticon and icons author reference', () => {
  const { getByText } = render(<Provider  store={store}><App /></Provider>);
  const flaticonRef = getByText(/Iconos diseñados por/i);
  expect(flaticonRef).toHaveTextContent('Iconos diseñados por Freepik from www.flaticon.es');
});
