import React, { useEffect } from "react";
import { useStore } from "react-redux";
import { fetchData } from "./actions";
import { BrowserRouter as Router } from "react-router-dom";

import ErrorList from "./components/ErrorList";
import Loader from "./components/Loader";
import FlaticonRef from "./components/FlaticonRef/FlaticonRef";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Routes from "./Routes";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.scss";

function App() {
  const store = useStore();

  useEffect(() => {
    store.dispatch(fetchData("cuba"));
  });

  return (
    <div className="App">
      <Router>
        <ErrorList />
        <Loader />
        <Header />
        <main className="App-wrapper container">
          <Routes />
          <FlaticonRef />
        </main>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
