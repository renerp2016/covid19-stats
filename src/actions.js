import axios from 'axios';
import {loading} from "./store/reducers";
import {
    ADD_ERROR,
    CLEAR_ERROR,
    CLEAR_ERRORS,
    RETRIEVE_COUNTRY_DATA,
    RETRIEVE_COUNTRY_DATA_FULFILLED,
    CANCEL_FETCHING,
    ADD_COUNTRY
}
    from './constants';

export const fetchData = country => (dispatch) => {
    dispatch({
        type: RETRIEVE_COUNTRY_DATA,
    });
    dispatch(loading);
    axios({
        'method': 'GET',
        'url': 'https://covid-193.p.rapidapi.com/statistics',
        'headers': {
            'content-type': 'application/octet-stream',
            'x-rapidapi-host': 'covid-193.p.rapidapi.com',
            'x-rapidapi-key': '6009a1a2e2msh0725eff496f16cfp1ec1bbjsn5c445a9a071d'
        }, 'params': {
            'country': country
        }
    })
        .then(res => {
            const {response} = res.data;
            const [data] = response;
            dispatch(updateCountryData(data));
            dispatch(cancelFetching());
            dispatch(clearErrors());
        })
        .catch(error => {
            const {message} = error;
            dispatch(addError(message));
            dispatch(cancelFetching());
        });
};

export const cancelFetching = () => ({type: CANCEL_FETCHING});
export const addError = (message) => ({type: ADD_ERROR, payload: message});
export const clearError = (index) => ({type: CLEAR_ERROR, payload: index});
export const clearErrors = (index) => ({type: CLEAR_ERRORS});
export const updateCountryData = (data) => ({type: RETRIEVE_COUNTRY_DATA_FULFILLED, payload: data});
export const addCountry = (name) => ({type: ADD_COUNTRY, payload: name});